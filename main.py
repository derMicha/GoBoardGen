# https://github.com/mozman/svgwrite
import svgwrite
from svgwrite.extensions import Inkscape

# use 9, 13, 15 or 19
size = 13
cellHeight = 22
cellWidth = 24
margin = 20

strokeBlack = svgwrite.rgb(0, 0, 0, '%')

boardWidth = (size - 1) * cellWidth
boardHeight = (size - 1) * cellHeight

boardWidthTotal = boardWidth + (2 * margin)
boardHeightTotal = boardHeight + (2 * margin)

# init canvas
dwg = svgwrite.Drawing(f"goboard-{size}.svg", profile='tiny', size=(boardWidthTotal, boardHeightTotal))
inkscape = Inkscape(dwg)
frameLayer = inkscape.layer(label="frame", locked=True)
dwg.add(frameLayer)
boardLayer = inkscape.layer(label="board", locked=True)
dwg.add(boardLayer)

# lines
for y in range(0, size):
    boardLayer.add(dwg.line((margin, y * cellHeight + margin), (boardWidth + margin, y * cellHeight + margin),
                            stroke=strokeBlack))
    boardLayer.add(dwg.line((y * cellWidth + margin, margin), (y * cellWidth + margin, boardHeight + margin),
                            stroke=strokeBlack))

# corner
dotOffset = 3
boardLayer.add(dwg.circle(center=(dotOffset * cellWidth + margin, dotOffset * cellHeight + margin), r=4))
boardLayer.add(dwg.circle(center=(dotOffset * cellWidth + margin, (size - dotOffset - 1) * cellHeight + margin), r=4))
boardLayer.add(dwg.circle(center=((size - dotOffset - 1) * cellWidth + margin, dotOffset * cellHeight + margin), r=4))
boardLayer.add(
    dwg.circle(center=((size - dotOffset - 1) * cellWidth + margin, (size - dotOffset - 1) * cellHeight + margin), r=4))

# center
dotOffset = int(size / 2)
boardLayer.add(dwg.circle(center=(dotOffset * cellWidth + margin, dotOffset * cellHeight + margin), r=4))

# frame
frameLayer.add(dwg.line((0, 0), (boardWidthTotal, 0), stroke=strokeBlack))
frameLayer.add(dwg.line((0, 0), (0, boardHeightTotal), stroke=strokeBlack))
frameLayer.add(dwg.line((boardWidthTotal, 0), (boardWidthTotal, boardHeightTotal), stroke=strokeBlack))
frameLayer.add(dwg.line((0, boardHeightTotal), (boardWidthTotal, boardHeightTotal), stroke=strokeBlack))

dwg.save()
