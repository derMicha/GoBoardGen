
# Go Board Generator 

Simple Python programm that could be used to generate SVG based Go boards.

About the Bo board game: https://en.wikipedia.org/wiki/Go_(game)

This little programm could be used to generate a SVG based Go board layout. This could be used to create Go boards using a laser cutter. 

## Laser Cutter Hints

Layer Board -> mark
Layer Frame -> cut

